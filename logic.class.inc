<?php

/**
 * @file
 *   A big fat chained logic operator class.
 */

define('LOGIC_OPERATOR_AND', 'AND');
define('LOGIC_OPERATOR_OR', 'OR');
// define('LOGIC_OPERATOR_XOR', 'XOR');

class LogicClass {

  /**
   * Stored and added logical definition.
   *
   * @var array
   */
  protected $items = array();

  protected $operator = LOGIC_OPERATOR_AND;

  protected $not = FALSE;

  protected $item_callback = 'logicClass_default_itemcallback';

  /**
   * Default constructor.
   */
  public function __construct($items = array(), $operator = LOGIC_OPERATOR_AND) {
    $this->setOperator($operator);
    $this->addItems($items);
    return $this;
  }

  public function setOperator($op) {
    $this->operator = $op;
    return $this;
  }
  public function setNOT() {
    $this->not = TRUE;
    return $this;
  }

  public function addItems($items) {
    $this->items = array_merge($this->items, $items);
    return $this;
  }

  public function addOR($items) {
    $el = new LogicClass($items, LOGIC_OPERATOR_OR);
    $this->items[] = $el;
    return $this;
  }
  public function addAND($items) {
    $el = new LogicClass($items, LOGIC_OPERATOR_AND);
    $this->items[] = $el;
    return $this;
  }
  //public function addXOR($items) {
  //  $el = new LogicClass($items, LOGIC_OPERATOR_XOR);
  //  $this->items[] = $el;
  //  return $this;
  //}

  public function isTRUE($arguments = NULL) {
    return $this->execute($arguments);
  }
  public function isFALSE($arguments = NULL) {
    return !$this->execute($arguments);
  }

  public function execute($arguments = NULL) {
    $item_returns = array();
    foreach ($this->items as $key => $value) {
      // If the value is logicOperator object, ask it if it isTRUE().
      if (is_object($value) && is_a($value, 'logicClass')) {
        $is_true = $value->isTRUE($arguments);
      }
      // else call the item callback.
      else {
        $is_true = $this->item_callback($key, $value, $arguments);
      }

      $item_returns[$key] = $is_true;
      // If one item is false, AND is false too.
      if ($this->operator == LOGIC_OPERATOR_AND && !$is_true) {
        return (!$this->not) ? FALSE : TRUE;
      }
      // If one item is true, OR is true too.
      elseif ($this->operator == LOGIC_OPERATOR_OR && $is_true) {
        return (!$this->not) ? TRUE : FALSE;
      }
    }

    // If o item is false, AND is true.
    if ($this->operator == LOGIC_OPERATOR_AND) {
      return (!$this->not) ? TRUE : FALSE;
    }
    // If no item is true, OR is false.
    elseif ($this->operator == LOGIC_OPERATOR_OR) {
      return (!$this->not) ? FALSE : TRUE;
    }
  }

  protected function item_callback($key, $value, $arguments) {
    if (function_exists($this->item_callback)) {
      return call_user_func($this->item_callback, $key, $value, $arguments);
    }
  }

}



function logicClass_default_itemcallback($key, $value, $arguments) {
  $callback = $value;
  $arg = $key;
  $object = (object) $arguments;


  // Check on not.
  if (drupal_substr($callback, 0, 1) == '!') {
    $not = TRUE;
    $callback = drupal_substr($callback, 1);
  }
  else {
    $not = FALSE;
  }

  switch ($callback) {
    case 'empty':
      $ret = empty($object->$arg);
      break;
    case 'isset':
      $ret = isset($object->$arg);
      break;
    default:
      $valid = $callback($object->$arg);
      break;
  }
  return ($not) ? !$ret : $ret;
}
