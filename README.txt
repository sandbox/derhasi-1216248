This module provides some helper functions for ctools wizards.

Cache
-----
Some wrapper functions for cache to generate a context and session dependent ID.

ctools_wizard_helper_step_exists
--------------------------------
This functions check on the given $wizard definition, if a specified step is
defined. If not it redirects or returns the path of the first step.

ctools_wizard_helper_validate_requirements
------------------------------------------
This function checks against an object (most times this will be the cached object)
if some reuqirements are fullfilled, and if not, the function redirects to a
fallback step.

So in each step you can check if specific variables are given, and go back to a
specific step, or skip this step.

Validation is processed by applying the LogicClass.

Using requirements, eases rhe use of validation, as you do not have to check all
values in each step's validation callback. You also can avoid missing arguments
or add extended logic in non-linear forms.

LogicClass (logic.class.inc)
----------------------------
This provides a class to check a variable (e.g. object) against given requirements.
Requirements can be joined AND and OR.

Example.
<?php
$logic = new LogicClass(array('my_value' => '!empty', 'my_value2' => 'isset'))->isTRUE($object);
?>

This example returns TRUE if $object->my_value is not empty and $object->my_value2 is set.


ctools_wizard_helper_extended_title
-----------------------------------
This function extends the drupal title with the Step Title as suffix.



EXAMPLE:

<?php
/**
 * Form builder for the multistep mywizard procedure.
 */
function mywizard_wizard($step = NULL) {
  $form_info = array(
    'id' => 'mywizard',
    ...
    'order' => array(
      'step1' => wiwob_t('Select rate and payment method'),
      'step2' => wiwob_t('User data'),
      'step3' => wiwob_t('Payment method'),
      'step4' => wiwob_t('Summary & legal'),
      //'confirmation' => wiwob_t('Confirmation'),
    ),
    'forms' => array(
      'step1' => array(
        'form id' => 'step1_form',
      ),
      'step2' => array(
        'form id' => 'step2_form',
      ),
      'step3' => array(
        'form id' => 'step3_form',
      ),
      'step4' => array(
        'form id' => 'step4_form',
      ),
    ),
    'step requirements' => array(
      'my_value_or_my_text' => array(
        'operator' => 'OR',
        'not' => FALSE,
        'items' => array('my_value' => '!empty', 'my_text' => '!empty'),
        'warning message' => wiwob_t('Please either select my_value or my_text'),
        'steps' => array('step2', 'step3', 'step4'),
        'fallback step' => 'step1',
      ),
      'married_or_single' => array(
        'operator' => 'OR',
        'items' => array(new LogicClass(array('married' => '!empty', 'spouse_name' => '!empty')), 'married' => 'empty'),
        'warning message' => wiwob_t('If you are married you have to enter the name of your spouse.'),
        'steps' => array('step4'),
        'fallback step' => 'step3',
      ),
    ),
  );

  // Build or retrieve an up to date cache id.
  $object_id = ctools_wizard_helper_object_cache_id('mywizard');

  if (empty($step)) {
    $step = 'step1';
  }
  // This automatically gets defaults if there wasn't anything saved.
  $object = ctools_wizard_helper_cache_get('mywizard', $object_id);

  // Check step and maybe redirect.
  ctools_wizard_helper_step_exists($step, $form_info);
  ctools_wizard_helper_validate_requirements($step, $form_info, $object);

  // Set the extended title.
  ctools_wizard_helper_extended_title($step, $form_info);

  $form_state = array(
    // Put our object and ID into the form state cache so we can easily find
    // it.
    'object_id' => $object_id,
    'object' => &$object,
  );
  ctools_include('wizard');
  $form = ctools_wizard_multistep_form($form_info, $step, $form_state);
  return drupal_render($form);
}